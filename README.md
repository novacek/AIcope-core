# AIcope Core

A public space dedicated to the core open source R&D activities associated with the AIcope research project funded by a [GAMU Interdisciplinary grant](https://gamu.muni.cz/en/pro-vedce/interdisciplinary-research-projects) (04/2021-12/2023).

## Participants and Project Coordinators

- [Faculty of Informatics](https://www.fi.muni.cz), [Masaryk University](https://www.muni.cz) (coordinating institution; project PI: [Vit Novacek](https://www.fi.muni.cz/~novacek/))
- [Masaryk Memorial Cancer Institute](https://www.mou.cz) and [Faculty of Medicine](https://www.med.muni.cz), [Masaryk University](https://www.muni.cz) (participating institutions; project co-PI: [Jana Halamkova](https://www.mou.cz/zamestnanec/mudr-jana-halamkova-ph-d/pe902))

## Basic Information on the Project

In 2016, over half a million people were registered as oncological patients in the Czech Republic, of which 96,500 were newly diagnosed. 27,261 people died that year (based on  data from the Institute of Health Information and Statistics). This clearly shows the substantial impact cancer has not only on the healthcare system, but also on the whole society and economy. The additional problem is that while there are various efficacious treatments available for most cancers, many of them may drastically impact the entire patients’ life, each in their own way. To further reduce the societal burden of cancer, it is therefore crucial to pick the right type of treatment, not only based on the patient's biological profile but also on their preferences and general lifestyle.

Unfortunately, this is seldom feasible. One of the main reasons are the severe time constraints of contemporary clinical practice—it is virtually impossible to review the implications of various available treatments with the patients while taking both their biomedical and their lifestyle profiles into account. And this is the very real, pressing clinical need that motivates the AIcope project.

To address the need, we will:
- Collect, extract and preprocess data from oncological patient records and relevant public datasets on diseases, interventions and drugs.
- Integrate the preprocessed data in a uniform, semantically-interlinked resource (a knowledge graph) and augment it by inferred links.
- Develop question answering and visual exploration interfaces on top of the knowledge graph in a co-design process with doctors, patients and clinical psychologists.
- Evaluate the resulting decision support prototype by its preliminary deployment in clinical settings and comparison with the current practices in the treatment selection process.

While the project will deliver cutting-edge, internationally-relevant results on its own, it will also provide a platform for an ERC Synergy grant application or a similar follow-up involving leading Irish cancer biologists and biomedical AI researchers with whom the AIcope PI already collaborates. This will let us extend the project research agenda by using advanced AI techniques at the diagnosis stage as well, and thus deliver a comprehensive and highly competitive precision oncology solution.
